FROM fedora:32

LABEL maintainer="Cnes Taro <goldenfishtiger@gmail.com>"

ARG http_proxy
ARG https_proxy

RUN set -x \
	&& dnf install -y \
		openldap-servers \
		openldap-clients \
		sscg \
		diffutils \
	&& rm -rf /var/cache/dnf/* \
	&& dnf clean all

# git clone ldap しておくこと
COPY ldap /prep

RUN systemctl enable slapd

EXPOSE 389
EXPOSE 636

# Dockerfile 中の設定スクリプトを抽出するスクリプトを出力、実行
COPY Dockerfile .
RUN echo $'\
cat Dockerfile | sed -n \'/^##__BEGIN0/,/^##__END0/p\' | sed \'s/^#//\' > startup.sh\n\
cat Dockerfile | sed -n \'/^##__BEGIN1/,/^##__END1/p\' | sed \'s/^#//\' > prep.sh\n\
' > extract.sh && bash extract.sh

# docker-compose up の最後に実行される設定スクリプト
##__BEGIN0__startup.sh__
#
#	if [ ! -e /var/lib/pv/slapd.d ]; then
#		cp -av /etc/openldap/slapd.d /var/lib/pv
#		cp -av /var/lib/ldap /var/lib/pv
#	fi
#	rm -rfv /etc/openldap/slapd.d
#	rm -rfv /var/lib/ldap
#	ln -sv /var/lib/pv/slapd.d /etc/openldap/slapd.d
#	ln -sv /var/lib/pv/ldap /var/lib/ldap
#
#	bash prep.sh &
#
#	echo 'startup.sh done.'
#
##__END0__startup.sh__

##__BEGIN1__prep.sh__
#
#	test -e /var/lib/pv/.preped && exit
#
#	sleep 5
#
#	if [ -e /var/lib/pv/prep ]; then
#		cd /var/lib/pv
#		bash prep/prep.sh
#		cd -
#	else
#		echo '#'; echo '# root password set up.'
#		phash=`slappasswd -s $LDAP_ROOT_PASSWORD`
#		cat prep/init.ldif.sample | sed "s@^\(olcRootPW:\s*\).*@\\1$phash@" > prep/init.ldif
#		ldapmodify -Y EXTERNAL -H ldapi:/// -f prep/init.ldif
#		ldapsearch -LLL -Y EXTERNAL -H ldapi:/// -b 'olcDatabase={2}mdb,cn=config'
#
#		echo '#'; echo '# base schemas set up.'
#		ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/cosine.ldif
#		ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/nis.ldif
#		ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/openldap/schema/inetorgperson.ldif
#		ls -l /etc/openldap/slapd.d/cn\=config/cn\=schema
#
#		echo '#'; echo '# tls set up.'
#		sscg -q											\
#			--cert-file			/var/lib/pv/ldap.crt	\
#			--cert-key-file		/var/lib/pv/ldap.key	\
#			--ca-file			/var/lib/pv/ldap.crt	\
#			--lifetime			3650					\
#			--hostname			$LDAP_CERT_FQDN			\
#			--email				root@$LDAP_CERT_FQDN
#		chown ldap:ldap /var/lib/pv/ldap.key /var/lib/pv/ldap.crt
#		ldapmodify -Y EXTERNAL -H ldapi:/// -f prep/init_tls.ldif
#		ldapsearch -LLL -Y EXTERNAL -H ldapi:/// -b 'cn=config' | grep -e ^olcTLS
#
#		echo '#'; echo '# base ous set up.'
#		ldapadd -D 'cn=Manager,ou=general,dc=example,dc=com' -w $LDAP_ROOT_PASSWORD -H ldapi:/// -f prep/add_ou.ldif
#		ldapsearch -LLL -Y EXTERNAL -H ldapi:/// -b 'ou=general,dc=example,dc=com'
#
#		echo '#'; echo '# base users set up.'
#		ldapadd -D 'cn=Manager,ou=general,dc=example,dc=com' -w $LDAP_ROOT_PASSWORD -H ldapi:/// -f prep/add_user.ldif
#		ldapsearch -LLL -Y EXTERNAL -H ldapi:/// -b 'ou=general,dc=example,dc=com'
#
#		echo '#'; echo '# to change user password.'
#		echo "ldappasswd -D 'cn=Manager,ou=general,dc=example,dc=com' -W -H ldap://ldap:389 -S cn=user1,ou=People,ou=general,dc=example,dc=com"
#	fi
#
#	touch /var/lib/pv/.preped
#
#	echo 'prep.sh done.'
#
##__END1__prep.sh__

ENTRYPOINT ["bash", "-c", "bash startup.sh && /sbin/init"]

