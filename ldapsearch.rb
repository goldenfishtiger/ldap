#!/usr/bin/env ruby

require 'net/ldap'

ldap = Net::LDAP.new(
	:host	=> 'localhost',
#	:host	=> ENV['HOSTNAME'],
	:port	=> 10389,
#	:port	=> 10636,
#	:auth	=> {
#		:method			=> :simple,
#		:username		=> 'cn=Manager,ou=general,dc=example,dc=com',
#		:password		=> 'lrpasswd',
#	},
#	:encryption	=> {
#		:method			=> :start_tls,
#		:method			=> :simple_tls,
#		:tls_options	=> {
#			:verify_mode	=> OpenSSL::SSL::VERIFY_NONE,
#		},
#	},
)
results = ldap.search(
	base:	'ou=general,dc=example,dc=com',
).each {|result|
	result.each {|k, vs|
		k == :dn and puts
		vs.each {|v|
			puts('%s: %s' % [k, v])
		}
	}
}

__END__

